<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use app\models\playlist_songs;

class playlistsongs extends Controller
{
    public function displaySongs(){
        return DB::table('playlist_songs')->get();
    }

    //Upload File
    public function store(Request $request){

        $newSong = new Playlist_Songs();
        $newSong->song_id = $request->song_id;
        $newSong->playlist_id = $request->playlist_id;
        $newSong->save();
        return $newSong;
    }
}
