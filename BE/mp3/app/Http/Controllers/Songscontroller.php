<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use app\models\songs;

class Songscontroller extends Controller
{
    public function display_songs(){
        return DB::table('songs')->get();
    }

    //for upload
    public function store(Request $request){

        $newSongs = new Songs();
        $newSongs->title = $request->title;
        $newSongs->length = $request->length;
        $newSongs->artist = $request->artist;
        $newSongs->save();
        return $newSongs;
    }
}

