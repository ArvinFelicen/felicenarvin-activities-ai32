<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Songs;
use App\Http\Controllers\playlist_songs;
use App\Http\Controllers\playlists;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

//for songs route
Route::get('/songs', [Songs::class, 'display_songs']);
Route::post('/upload', [songs::class, 'store']);

//for Playlist route
Route::get('/playlist', [PlaylistController::class, 'displayPlaylists']);
Route::post('/create', [PlaylistController::class, 'store']);

//for Playlist_songs route
Route::get('/playlistsongs', [PlaylistSongController::class, 'displayPlaylistSongs']);
Route::post('/create', [PlaylistSongController::class, 'store']);